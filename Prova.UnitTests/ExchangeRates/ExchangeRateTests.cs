using FluentAssertions;
using Xunit.Abstractions;

namespace Prova.UnitTests.ExchangeRates
{
    public class ExchangeRateTests : IClassFixture<ExchangeRateTestsFixture>
    {
        private readonly ExchangeRateTestsFixture _fixture;
        private readonly ITestOutputHelper _outputHelper;

        public ExchangeRateTests(ExchangeRateTestsFixture fixture, ITestOutputHelper outputHelper)
        {
            _fixture = fixture;
            _outputHelper = outputHelper;
        }

        [Fact(DisplayName = "New Exchange Rate Should Round Factor To 5 Decimal Cases")]
        [Trait("Category", "Exchange Rate Tests")]
        public void NewExchangeRate_WhenParametersAreValid_ShouldRoundFactorToFiveDecimalCases()
        {
            // ARRANGE & ACT
            var exchangeRate = new ExchangeRate(
                DateOnly.FromDateTime(DateTime.Now).AddDays(-2),
                23.500009M,
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid());

            // ASSERT
            exchangeRate.Factor.Should().Be(23.50001M);
        }

        [Fact(DisplayName = "Validate Should Throw Exception When Factor Is Equal To Zero")]
        [Trait("Category", "Exchange Rate Tests")]
        public void Validate_WhenFactorIsZero_ShouldThrowDomainException()
        {
            // ARRANGE & ACT
            Action act = () => _fixture.GenerateInvalidFactorExchangeRates(1).FirstOrDefault();
            var exception = Record.Exception(act);

            // ASSERT
            exception.Should().BeOfType<DomainException>();
            exception.Message.Should().Be("The Factor cannot be zero");
            _outputHelper.WriteLine($"Exception Message : {exception.Message}");
        }

        [Fact(DisplayName = "Validate Should Throw Exception When Type Id Is Invalid")]
        [Trait("Category", "Exchange Rate Tests")]
        public void Validate_WhenTypeIdIsInvalid_ShouldThrowDomainException()
        {
            // ARRANGE & ACT
            Action act = () => _fixture.GenerateInvalidTypeIdExchangeRates(1).FirstOrDefault();
            var exception = Record.Exception(act);

            // ASSERT
            exception.Should().BeOfType<DomainException>();
            exception.Message.Should().Be("The TypeId cannot be empty");
            _outputHelper.WriteLine($"Exception Message : {exception.Message}");
        }

        [Fact(DisplayName = "Validate Should Throw Exception When Quotation Date Is In The Future")]
        [Trait("Category", "Exchange Rate Tests")]
        public void Validate_WhenQotationDateIsInTheFuture_ShouldThrow()
        {
            // ARRANGE & ACT
            Action act = () => _fixture.GenerateInvalidQuotationDateExchangeRates(1).FirstOrDefault();
            var exception = Record.Exception(act);

            // ASSERT
            exception.Should().BeOfType<DomainException>();
            exception.Message.Should().Be("The QuotationDate cannot be in the future");
            _outputHelper.WriteLine($"Exception Message : {exception.Message}");
        }
    }
}