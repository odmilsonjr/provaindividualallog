﻿
using Bogus;
using Prova;

namespace Prova.UnitTests.ExchangeRates
{
    public class ExchangeRateTestsFixture
    {
        public List<ExchangeRate> GenerateValidExchangeRates(int quantity)
        {
            var exchangeRate = new Faker<ExchangeRate>()
                .CustomInstantiator(f => new ExchangeRate(
                    f.Date.PastDateOnly(1, DateOnly.FromDateTime(DateTime.Now)),
                    23.500009M,
                    Guid.NewGuid(),
                    Guid.NewGuid(),
                    Guid.NewGuid()
                ));
            return exchangeRate.Generate(quantity);
        }

        public List<ExchangeRate> GenerateInvalidFactorExchangeRates(int quantity)
        {
            var exchangeRate = new Faker<ExchangeRate>()
                .CustomInstantiator(f => new ExchangeRate(
                    f.Date.PastDateOnly(1, DateOnly.FromDateTime(DateTime.Now).AddDays(-2)),
                    0M,
                    Guid.NewGuid(),
                    Guid.NewGuid(),
                    Guid.NewGuid()
                ));
            return exchangeRate.Generate(quantity);
        }

        public List<ExchangeRate> GenerateInvalidTypeIdExchangeRates(int quantity)
        {
            var exchangeRate = new Faker<ExchangeRate>()
                .CustomInstantiator(f => new ExchangeRate(
                    f.Date.PastDateOnly(1, DateOnly.FromDateTime(DateTime.Now).AddDays(-2)),
                    23.500009M,
                    Guid.Empty,
                    Guid.NewGuid(),
                    Guid.NewGuid()
                ));
            return exchangeRate.Generate(quantity);
        }

        public List<ExchangeRate> GenerateInvalidQuotationDateExchangeRates(int quantity)
        {
            var exchangeRate = new Faker<ExchangeRate>()
                .CustomInstantiator(f => new ExchangeRate(
                    f.Date.FutureDateOnly(1, DateOnly.FromDateTime(DateTime.Now).AddDays(2)),
                    23.500009M,
                    Guid.NewGuid(),
                    Guid.NewGuid(),
                    Guid.NewGuid()
                ));
            return exchangeRate.Generate(quantity);
        }
    }
}
